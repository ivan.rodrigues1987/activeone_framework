
Feature: C|A - Release 3.6.0.0
  New HFRI [CMD-1224] and Bloomberg [CMD-1331] indices


  Scenario: [CMD-1324] - Database setting validation to the job: HFRI
    Given I have a new "Exposure Data" job into CMDB database
    When the "HFRI" job is set at "BenchmarkSetConfig"
    Then results should match on "RunasPreviousMonthLastBusinessDay" and "FIVEDAYS" and "US"


  Scenario: [CMD-1324] - Padding validation for: HFRI
    Given I have a new "Exposure Data" job into CMDB database
    When the "'HFRIEHI', 'HFRIFOF', 'HFRIFOFD'" job is set at "Benchmark"


  Scenario: [CMD-1344] - Database setting validation to the job: BBGC
    Given I have a new "Exposure Data" job into CMDB database
    When the "BBGC" job is set at "BenchmarkSetConfig"
    Then results should match on "RunAsPreviousBusinessDay" and "US" and "US"

  Scenario: [CMD-1324] - Padding validation for: BBGC
    Given I have a new "Exposure Data" job into CMDB database
    When the "'BBGCICMP'" job is set at "Benchmark"


  Scenario: [CMD-XXXX] - Constituent File - Roll Up to 100%:
    Given I have a new "Exposure Data" job into CMDB database
    When the BenchmarkId is "000001EQ", FamilyId is "SPUS", Currency is "USD" and Date is "2019-09-26"
    Then the benchmarkId into the Constituent file should roll up to 100


  Scenario: [CMD-1344] - Database setting validation to the job: BSFI
    Given I have a new "Exposure Data" job into CMDB database
    When the "BSFI" job is set at "BenchmarkSetConfig"
    Then results should match on "RunAsPreviousBusinessDay" and "NewYear" and "US"

  Scenario: [CMD-1324] - Padding validation for: BSFI (1/2)
    Given I have a new "Exposure Data" job into CMDB database
    When the "'LHMN0001', 'LHMN0003', 'LHMN0004', 'LHMN0005', 'LHMN0038', 'LHMN0151', 'LHMN0152'" job is set at "Benchmark"

  Scenario: [CMD-1324] - Padding validation for: BSFI (2/2)
    Given I have a new "Exposure Data" job into CMDB database
    When the "'LHMN1302', 'LHMN1303', 'LHMN20326', 'LHMN21894', 'LHMN23034', 'LHMN24091', 'LHMN2765'" job is set at "Benchmark"





