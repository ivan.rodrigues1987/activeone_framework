#language: pt

Funcionalidade: Dev RSL - VIVO

  @CA
  Cenário: TC001: - VIVO RSL - Cadastro de Novo Usuário
    Dado Eu queira entrar com um "Novo Cliente" na Loja VIVO
    Quando a tela de cadastro for exibida eu devo entrar com os Dados Pessoais
      |CPF             |NomePessoal            |Sobrenome            |EmailPessoal     |TelefonePessoal|
      |<autoGeneration>|AutoTeste Vivo         |Teste Vivo Sobrenome |<autoGeneration> |1180811987     |
    E depois eu devo entrar os Dados da Empresa
      |CNPJ              |NomeEmpresa         |InscricaoEstadual|RuaEmpresa              |NumEndEmpresa|Complemento|Bairro         |TelefoneEmpresa|Estado |Cidade    |CEP      |
      | <autoGeneration> |AutoTest GW Vivo SA | 813928193884    | Rua Endereco da Empresa| 1420        | Bloco C   | Jardim Empresa|11 35452158    |SP     | Sao Paulo| 02316220|
    E ao logar na conta de email abra o link para Alteração de Senha enviado no fim do cadastro
    Então ao definir a senha e os demais dados solicitados clique em "Salvar e Finalizar"


  @CA
  Cenário: TC002: - VIVO RSL - Comprando um produto SaaS - Usuário Existente
    Dado Eu queira entrar com um "Usuário Existente" na Loja VIVO
    Quando Eu acessar o menu "Catálogo" e o filtro "SaaS" e "Gerenciamento"
    E ao selecionar o produto "Microsoft 365" e depois "Microsoft 365 F1"
    E seguir com os itens "Finalizar Compra"
    E validar em "Continuar" e "Prosseguir"
    Então confirmar o pagamento pela mensagem "Obrigado pelo seu pedido. Um e-mail de confirmação do pedido foi enviado para você. Visite sua página de inscrições de conta para verificar o status do seu pedido."


