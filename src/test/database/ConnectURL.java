import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectURL {
    public static void main(String[] args) {

        // Create a variable for the connection string.
        String connectionUrl = "jdbc:sqlserver://DTC01-D-WEB01:1433;databaseName=CMDB;user=irodrigues;password=Reptilianos10";

        try (Connection con = DriverManager.getConnection(connectionUrl); Statement stmt = con.createStatement();) {
            String SQL = "SELECT TOP 10 * FROM ClassificationTagMappings";
            ResultSet rs = stmt.executeQuery(SQL);

            System.out.prinln("Connected");
            // Iterate through the data in the result set and display it.
            while (rs.next()) {
                System.out.println(rs.getString("ClassificationMappingRowId") + " " + rs.getString("ClassificationRowId") + " " + rs.getString("ClassificationTagRowId"));
            }
        }
        // Handle any errors that may have occurred.
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}