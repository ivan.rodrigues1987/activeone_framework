package gherkin.stepdefinition;

import br.com.core.asserts.Verifications;
import br.com.core.asserts.CAValidations;
import br.com.core.database.DataBases;
import br.com.core.enums.DbPlatform;
import br.com.core.ftp.SftpManager;
import br.com.core.report.ExtentReports;
import br.com.core.setup.AppWeb;
import br.com.core.setup.DriverManager;
import br.com.pom.interfaces.Environment;
import br.com.pom.model.QueryDefault;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;



public class WebServicesCA extends DriverManager implements Environment {

    String uriPath;

    @Given("I have the following uri {string}")
    public void iHaveTheFollowingUri(String uri) {
        uriPath = uri;
        ExtentReports.appendToReport("Request: " + endPointDevCA + uri);
    }

    //INDEX RATE ONLY
    @When("jobType {string} and asOfDate {string} are requested")
    public void jobtypeAndAsOfDateAreRequested(String jobType, String asOfDate) {
        CAValidations.hashIndexRate.put("jobType", jobType);
        CAValidations.hashIndexRate.put("asOfDate", asOfDate);
    }

    //EXPOSURE DATA ONLY
    @When("benchmarkSet {string} and asOfDate {string} are requested")
    public void benchmarksetidAndAsOfDateAreRequested(String benchmarkSet, String asOfDate) {
        CAValidations.hashExposureData.put("benchmarkSet", benchmarkSet);
        CAValidations.hashExposureData.put("asOfDate", asOfDate);
    }



    @Then("the HTTP Status {int} is returned and the message is {string} to the {string}")
    public void theHTTPStatusIsReturnedAndTheMessageIsToThe(int httpStatus, String httpMessage, String typeOfTheJob) {

        Map<String, Object> headers = new HashMap<String, Object>();

       if (typeOfTheJob.equals("Index Rate")) {
           headers.put("Content-Type", "application/json");
           CAValidations.responseIndexRate = CAValidations.restAssuredIndexRate.given()
                   .queryParams(CAValidations.hashIndexRate)
                   .auth().ntlm("irodrigues", "Powerslave84", "", "")
                   .headers(headers)
                   .post(endPointDevCA + uriPath);

           if (httpMessage.equals("Job enqueued Successfully")) {

               boolean status = CAValidations.responseIndexRate.jsonPath().get("Successful");
               String message = CAValidations.responseIndexRate.jsonPath().get("Message");
               ExtentReports.appendToReport("Response: " + CAValidations.responseIndexRate.getBody().prettyPrint());
           }
           else {
               throw new java.lang.Error(CAValidations.responseIndexRate.getBody().prettyPrint());
               //String message = CAValidations.responseExposureData.jsonPath().get("Message");
               //ExtentReports.appendToReport("Response: " + CAValidations.responseIndexRate.getBody().prettyPrint());

           }

       } else
       {
           headers.put("Content-Type", "application/json");
           CAValidations.responseExposureData = CAValidations.restAssuredExposureData.given()
                   .queryParams(CAValidations.hashExposureData)
                   .auth().ntlm("irodrigues", "Powerslave84", "", "")
                   .headers(headers)
                   .post(endPointDevCA + uriPath);

           if (httpMessage.equals("Job enqueued Successfully")) {

               boolean status = CAValidations.responseExposureData.jsonPath().get("Successful");
               String message = CAValidations.responseExposureData.jsonPath().get("Message");
               ExtentReports.appendToReport("Response: " + CAValidations.responseExposureData.getBody().prettyPrint());

           }
           else
           {
               //String message = CAValidations.responseExposureData.jsonPath().get("Message");
               throw new java.lang.Error(CAValidations.responseExposureData.getBody().prettyPrint());
           }
       }




    }

    @Then("{string} job will be {string}")
    public void jobWillBe(String typeOfJob, String responseStatus) {

        CAValidations.hashIndexRate.put("IndexRate", typeOfJob);
        CAValidations.hashExposureData.put("ExposureData", typeOfJob);

        if (uriPath.equals("/datastream/Api/Jobs/Run")) {
            if (CAValidations.hashIndexRate.get("jobType").equals("DailyDelayed")) {
                Verifications.wait(2);
            } else {
                Verifications.wait(200);
            }
        } else {
            Verifications.wait(15);
        }

        AppWeb web = new AppWeb();
        web.setBrowserName("headless");
        web.setUpDriver(web);


        switch (responseStatus) {
            case "Succeeded":
                getBrowser().get(hangfireDevCA + "jobs/succeeded");
                break;
            case "Failed":
                getBrowser().get(hangfireDevCA + "jobs/failed");
                break;
            case "Processing":
                getBrowser().get(hangfireDevCA + "jobs/processing");
                break;
            case "Scheduled":
                getBrowser().get(hangfireDevCA + "jobs/scheduled");
                break;
            case "Enqueued":
                getBrowser().get(hangfireDevCA + "jobs/enqueued");
                break;
        }

        if (typeOfJob.equals("Index Rate")) {
            int countRows = getBrowser().findElements(By.xpath("//tbody/tr")).size() + 1;
            for (int i = 1; i < getBrowser().findElements(By.xpath("//tbody/tr")).size() + 1; i++) {
                if (getBrowser().findElement(By.xpath("//tbody/tr[" + i + "]/td[3]")).getText().contains(("IndexRate - " + CAValidations.hashIndexRate.get("jobType") + " - " + CAValidations.hashIndexRate.get("asOfDate")))) {
                    Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.xpath("//tbody/tr[" + i + "]")));
                    break;
                }
            }
        } else {
            int countRows = getBrowser().findElements(By.xpath("//tbody/tr")).size() + 1;
            for (int i = 1; i < getBrowser().findElements(By.xpath("//tbody/tr")).size() + 1; i++) {
                if (getBrowser().findElement(By.xpath("//tbody/tr[" + i + "]/td[3]")).getText().contains(("ExposureData - " + CAValidations.hashExposureData.get("benchmarkSet") + " - " + CAValidations.hashExposureData.get("asOfDate")))) {
                    Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.xpath("//tbody/tr[" + i + "]")));
                    break;
                }
            }
        }

        ExtentReports.appendToReport(getBrowser());
    }

    @Given("I have a new {string} job into CMDB database")
    public void iHaveANewJobIntoCMDBDatabase(String type) {
        CAValidations.hashJobType.put("JobType", type);
    }


    @When("the {string} job is set at {string}")
    public void theJobIsSetAt(String jobName, String tableName) throws SQLException {
        //OPENS DATABASE CONNECTION
        DataBases db = new DataBases();
        db.openConnection(DbPlatform.SQLSERVER);

        //OBJECT TO RETRIEVE THE QUERIES FROM THE QUERY PageObject CLASS.
        QueryDefault call = new QueryDefault();
        call.shortBenchmarkSetConfig(jobName);


        CAValidations.hashTableName.put("BenchmarkSetConfig", tableName);
        CAValidations.hashTableName.put("Benchmark", tableName);

        if (tableName.equals("BenchmarkSetConfig")) {
            if (CAValidations.hashJobType.get("JobType").equals("Exposure Data")) {

                ResultSet rs = db.select(call.shortBenchmarkSetConfig(jobName));

                while (rs.next()) {
                    ExtentReports.appendToReport("BenchmarkSetId: " + rs.getString(1));
                    CAValidations.hashResultSet.put("BenchmarkSetId", rs.getString(1));
                    ExtentReports.appendToReport("source_Id: " + rs.getString(2));
                    CAValidations.hashResultSet.put("source_id", rs.getString(2));
                    ExtentReports.appendToReport("vendor_id: " + rs.getString(3));
                    CAValidations.hashResultSet.put("vendor_id", rs.getString(3));
                    ExtentReports.appendToReport("isActive: " + rs.getString(4));
                    CAValidations.hashResultSet.put("isActive", rs.getString(4));
                    ExtentReports.appendToReport("FrequencyTypeId: " + rs.getString(5));
                    CAValidations.hashResultSet.put("FrequencyTypeId", rs.getString(5));
                    ExtentReports.appendToReport("Day: " + rs.getString(6));
                    CAValidations.hashResultSet.put("Day", rs.getString(6));
                    ExtentReports.appendToReport("ReferenceAsOfDateTypeId: " + rs.getString(7));
                    CAValidations.hashResultSet.put("ReferenceAsOfDateTypeId", rs.getString(7));
                    ExtentReports.appendToReport("TimeOfDay: " + rs.getString(8));
                    CAValidations.hashResultSet.put("TimeOfDay", rs.getString(8));
                    ExtentReports.appendToReport("CenterCode: " + rs.getString(9));
                    CAValidations.hashResultSet.put("CenterCode", rs.getString(9));
                    ExtentReports.appendToReport("CountryCode: " + rs.getString(10));
                    CAValidations.hashResultSet.put("CountryCode", rs.getString(10));
                    ExtentReports.appendToReport("-----------------------------------------------------------------");

                }
            }
        } else if (tableName.equals("Benchmark")) {
            ResultSet rs = db.select(call.shortBenchmark(jobName));
            while (rs.next()) {
                ExtentReports.appendToReport("BenchmarkId: " + rs.getString(1));
                CAValidations.hashResultSet.put("BenchmarkId", rs.getString(1));
                ExtentReports.appendToReport("Date: " + rs.getString(2));
                CAValidations.hashResultSet.put("Date", rs.getString(2));
                ExtentReports.appendToReport("Currency: " + rs.getString(3));
                CAValidations.hashResultSet.put("Currency", rs.getString(3));
                ExtentReports.appendToReport("Name: " + rs.getString(4));
                CAValidations.hashResultSet.put("Name", rs.getString(4));
                ExtentReports.appendToReport("MarketCap: " + rs.getString(5));
                CAValidations.hashResultSet.put("MarketCap", rs.getString(5));
                ExtentReports.appendToReport("Price: " + rs.getString(6));
                CAValidations.hashResultSet.put("Price", rs.getString(6));
                ExtentReports.appendToReport("EntrySourceId: " + rs.getString(7));
                CAValidations.hashResultSet.put("EntrySourceId", rs.getString(7));
                ExtentReports.appendToReport("-----------------------------------------------------------------");
            }
        }
    }


    @Then("results should match on {string} and {string} and {string}")
    public void resultsShouldMatchOnAndAnd(String expectedValue1, String expectedValue2, String expectedValue3) {

        if ((CAValidations.hashResultSet.get("ReferenceAsOfDateTypeId").equals(expectedValue1)) && (CAValidations.hashResultSet.get("CenterCode").equals(expectedValue2)) && (CAValidations.hashResultSet.get("CountryCode").equals(expectedValue3))) {
            ExtentReports.appendToReport("SUCCESSFUL COMPARISON: ");
            ExtentReports.appendToReport("Reference AsOfDate TypeId: " + expectedValue1);
            ExtentReports.appendToReport("Center Code: " + expectedValue2);
            ExtentReports.appendToReport("Country Code: " + expectedValue3);
        } else {
            throw new java.lang.Error("DATABASE VALUES DO NOT MATCH THE PARAMETERS - " + CAValidations.hashResultSet.get("ReferenceAsOfDateTypeId") + " - " + CAValidations.hashResultSet.get("CenterCode") + " - " + CAValidations.hashResultSet.get("CountryCode"));
        }
    }


    @When("the BenchmarkId is {string}, FamilyId is {string}, Currency is {string} and Date is {string}")
    public void theBenchmarkIdIsFamilyIdIsCurrencyIsAndDateIs(String benchmarkId, String familyId, String currency, String date) throws SQLException {
        //OPENS DATABASE CONNECTION
        DataBases db = new DataBases();
        db.openConnection(DbPlatform.SQLSERVER);

        //OBJECT TO RETRIEVE THE QUERIES FROM THE QUERY PageObject CLASS.
        QueryDefault call = new QueryDefault();
        call.rollUpConstituent(benchmarkId, familyId, currency, date);

        ResultSet rs = db.select(call.rollUpConstituent(benchmarkId, familyId, currency, date));

        while (rs.next()) {
            ExtentReports.appendToReport("BenchmarkSetId: " + rs.getString(1));
            CAValidations.hashResultSet.put("BenchmarkSetId", rs.getString(1));

            ExtentReports.appendToReport("BenchmarkId: " + rs.getString(2));
            CAValidations.hashResultSet.put("BenchmarkId", rs.getString(2));

            ExtentReports.appendToReport("FamilyId: " + rs.getString(3));
            CAValidations.hashResultSet.put("FamilyId", rs.getString(3));

            ExtentReports.appendToReport("Currency: " + rs.getString(4));
            CAValidations.hashResultSet.put("Currency", rs.getString(4));

            ExtentReports.appendToReport("Date: " + rs.getString(5));
            CAValidations.hashResultSet.put("Date", rs.getString(5));

            ExtentReports.appendToReport("Roll up to: " + rs.getString(6));
            CAValidations.hashResultSet.put("RollUpto", rs.getString(6));

        }
    }

    @Then("the benchmarkId into the Constituent file should roll up to {int}")
    public void theBenchmarkIdIntoTheConstituentFileShouldRollUpTo(int rollUp) {

        double rollUpDouble = Double.parseDouble(CAValidations.hashResultSet.get("RollUpto").toString());
        String rollUpString = Double.toString(rollUpDouble);

        if (rollUpDouble >= 99 || rollUpDouble <= 101) {
            ExtentReports.appendToReport("Constituent Level Roll up: " + rollUpString);
        } else {
            throw new java.lang.Error("Constituent Level Data roll up is out of the expected range: " + rollUpString);
        }

    }


    @And("the jobId should be get from the execution above")
    public void theJobIdShouldBeGetFromTheExecutionAbove() throws SQLException {

        CAValidations.getJobId();

    }

    @Then("the JobId is provided the expected Status is: {string}")
    public void theJobIdIsProvidedTheExpectedStatusIs(String status) throws SQLException {
        DataBases db = new DataBases();
        db.openConnection(DbPlatform.SQLSERVER);

            QueryDefault call = new QueryDefault();
            ResultSet rs = db.select(call.checkApplicationLog(CAValidations.hashResultSet.get("getJobId").toString(), status));

            while (rs.next()) {
                ExtentReports.appendToReport("LogId: " + rs.getString(1));
                CAValidations.hashResultSet.put("LogId", rs.getString(1));
                ExtentReports.appendToReport("EntryDate: " + rs.getString(2));
                CAValidations.hashResultSet.put("EntryDate", rs.getString(2));
                ExtentReports.appendToReport("Origin: " + rs.getString(3));
                CAValidations.hashResultSet.put("Origin", rs.getString(3));
                ExtentReports.appendToReport("LogLevel: " + rs.getString(4));
                CAValidations.hashResultSet.put("LogLevel", rs.getString(4));
                ExtentReports.appendToReport("Message: " + rs.getString(5));
                CAValidations.hashResultSet.put("Message", rs.getString(5));
                ExtentReports.appendToReport("JobId: " + rs.getString(6));
                CAValidations.hashResultSet.put("JobId", rs.getString(6));
                ExtentReports.appendToReport("-----------------------------------------------------------------");

            }

        }


}