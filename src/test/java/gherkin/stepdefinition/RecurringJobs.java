package gherkin.stepdefinition;

import br.com.core.asserts.Verifications;
import br.com.core.enums.DbPlatform;
import br.com.core.report.ExtentReports;
import br.com.core.setup.AppWeb;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.pom.interfaces.Environment;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import de.retest.recheck.ui.actions.Actions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import br.com.core.database.*;

import java.util.HashMap;
import java.util.Map;

public class RecurringJobs extends DriverManager implements Environment {
    WebDriver driver;
    public static Map<String, Object> hashJobName = new HashMap<String, Object>();


    public void ConnectionSQLServer(){
        DataBases db = new DataBases ("localhost", "", "", "", "");
            db.openConnection(DbPlatform.ORACLE);
    }

    @Given("I need to access {string} page")
    public void iNeedToAccessPage(String tabOption) {

        AppWeb web = new AppWeb();
        web.setBrowserName("Chrome");
        web.setUpDriver(web);

        switch (tabOption){
            case "Recurring Jobs":
                getBrowser().get(hangfireDevCA + "recurring");
                break;
            case "Retries":
                getBrowser().get(hangfireDevCA + "retries");
                break;
        }
    }

    @When("I check {string} job and hit {string} button")
    public void iCheckJobAndHitButton(String idJob, String btnRecurring) {
        hashJobName.put("job", idJob);

        Action.clickOnElement(getBrowser(), By.xpath("//td[@class='min-width'][contains(text(),'"+idJob+"')]"), 1);
        ExtentReports.appendToReport(getBrowser());

        Action.clickOnElement(getBrowser(), By.xpath("//button[@class='js-jobs-list-command btn btn-sm btn-primary']"), 1);
    }

    @Then("confirm if the job {string}")
    public void confirmIfTheJob(String msgReturned) {

        if (hashJobName.get("job").equals("Autopost - Daily")){
            Verifications.wait(17);
        }
        else
        {
            Verifications.wait(3);
        }

        switch (msgReturned) {
            case "Succeeded":
                getBrowser().get(hangfireDevCA + "jobs/succeeded");
                break;
            case "Failed":
                getBrowser().get(hangfireDevCA + "jobs/failed");
                break;
            case "Processing":
                getBrowser().get(hangfireDevCA + "jobs/processing");
                break;
            case "Scheduled":
                getBrowser().get(hangfireDevCA + "jobs/scheduled");
                break;
            case "Enqueued":
                getBrowser().get(hangfireDevCA + "jobs/enqueued");
                break;
        }


        String getJobName = Action.getText(getBrowser(), By.xpath("//tbody/tr[1]/td[3]"), 1);
        if (getJobName.equals(hashJobName.get("job"))){
            Verifications.verifyTextsExistingElement(getBrowser(), By.xpath("//tbody/tr[1]/td[3]"), getJobName, 1);
        }

        Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.xpath("//tbody/tr[1]")));
        ExtentReports.appendToReport(getBrowser());
    }
}
