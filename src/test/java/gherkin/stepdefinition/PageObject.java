package gherkin.stepdefinition;

import br.com.core.asserts.Verifications;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.Map;



public class PageObject {

   private static WebElement element = null;

    public static  WebElement TCadastroLblNovoCliente(WebDriver driver)
    {
        WebElement lblNovoCliente = driver.findElement(By.xpath("//h2[@class='clearboth marginLeft-25-IE'][contains(text(),'Novo Cliente')]"));
        return lblNovoCliente;

    }

    public static WebElement TCadastroLblEmpresa (WebDriver driver){
        WebElement lblEmpresa = driver.findElement(By.xpath("//div[@class='headingText padding-10'][contains(text(),'Empresa')]"));
        return lblEmpresa;
    }

}
