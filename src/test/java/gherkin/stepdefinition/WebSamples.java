package gherkin.stepdefinition;

import br.com.core.asserts.Verifications;
import br.com.core.report.ExtentReports;
import br.com.core.setup.AppWeb;
import br.com.core.setup.DriverManager;
import static br.com.core.view.Action.*;


import br.com.pom.interfaces.Environment;
import cucumber.api.PendingException;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class WebSamples extends DriverManager implements Environment {

    public String novoEmail = "vivo_automation5032@mailinator.com";
    public String existenteEmail = "vivoautoteste@protonmail.com";
    public String txtMicrosoftDomain = "microsoftId" + randomNum(3);

    /*-----------------------------------------------------------------------------------------------------------------*/


    public static Map<String, Object> hashPessoaFisica = new HashMap<>();
    public static Map<String, Object> hashUsuarios = new HashMap<>();

    @Dado("^Eu queira entrar com um \"([^\"]*)\" na Loja VIVO$")
    public void euQueiraEntrarComUmNaLojaVIVO(String tipoCliente) {

        AppWeb web = new AppWeb();
        web.setBrowserName("Chrome");
        web.setUpDriver(web);

        hashUsuarios.put("userExistente", tipoCliente.equals("Usuário Existente"));
        hashUsuarios.put("userNovo", tipoCliente.equals("Novo Cliente"));


        if (tipoCliente.equals("Novo Cliente")) {

            getBrowser().get(DevRslVivo + "jsdn/register/cmsSelfRegister.action?fromSelfRegistration=true");
            Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.xpath("//h2[@class='clearboth marginLeft-25-IE'][contains(text(),'Novo Cliente')]")));
            Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.xpath("//div[@class='headingText padding-10'][contains(text(),'Empresa')]")));
            ExtentReports.appendToReport(getBrowser());


        } else if (tipoCliente.equals("Usuário Existente")) {

            getBrowser().get(DevRslVivo);
            clickOnElement(getBrowser(), By.xpath("//a[contains(text(),'Entrar')]"), 10);

            //CREDENCIAIS
            //setText(getBrowser(), By.id("edit-name"), "vivotesteberrini@mailinator.com", 10);
            setText(getBrowser(), By.id("edit-name"), existenteEmail, 10);
            setText(getBrowser(), By.id("edit-pass"), "Vivoteste2020", 10);

            //Botão ENTRAR
            clickOnElement(getBrowser(), By.id("edit-submit"), 10);

            //Verifications.wait(3);
            //Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.className("floatRight")));
            ExtentReports.appendToReport(getBrowser());
        }
    }


    @Quando("^a tela de cadastro for exibida eu devo entrar com os Dados Pessoais$")
    public void aTelaDeCadastroForExibidaEuDevoEntrarComOsDadosPessoais(DataTable datatablePessoal) {

        List<Map<String, String>> tablePessoaFisica = datatablePessoal.asMaps();

        for (Map<String, String> formValidarNovoCadastro : tablePessoaFisica) {

            //Validar se o e-mail do Cadastro já existe
            setText(getBrowser(), By.id("cpfNumber"), gerarCPF(), 10);
            clickOnElement(getBrowser(), By.id("firstName"), 10);

            try {
                for (Map<String, String> formPessoal : tablePessoaFisica) {

                    String setEmailAddress = "vivo_automation" + randomNum(4) + "@mailinator.com";
                    hashPessoaFisica.put("hashEmail", setEmailAddress);

                    setText(getBrowser(), By.id("firstName"), formPessoal.get("NomePessoal"), 10);
                    setText(getBrowser(), By.id("lastName"), formPessoal.get("Sobrenome"), 10);
                    setText(getBrowser(), By.id("email"), setEmailAddress, 10); //" + randomNum(1) + "
                    setText(getBrowser(), By.id("contactPhone"), formPessoal.get("TelefonePessoal"), 10);

                }
            } catch (Exception e) {
                Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.id("uniqueValidationErrors")));
                ExtentReports.appendToReport(getBrowser());
                throw new java.lang.Error(getText(getBrowser(), By.id("uniqueValidationErrors"), 10));
            }
        }
    }

    @E("^depois eu devo entrar os Dados da Empresa$")
    public void depoisEuDevoEntrarOsDadosDaEmpresa(DataTable dataTableEmpresa) {

        List<Map<String, String>> tablePessoaJuridica = dataTableEmpresa.asMaps();

        for (Map<String, String> formEmpresa : tablePessoaJuridica) {
            setText(getBrowser(), By.id("cnpjNumber"), gerarCNPJ(), 10);
            setText(getBrowser(), By.id("companyName"), "vivoEmpresa " + formEmpresa.get("NomeEmpresa"), 10);
            setText(getBrowser(), By.id("stateRegister"), formEmpresa.get("InscricaoEstadual"), 10);
            setText(getBrowser(), By.id("mailingAddress1"), formEmpresa.get("RuaEmpresa"), 10);
            setText(getBrowser(), By.id("mailingAddress2"), formEmpresa.get("NumEndEmpresa"), 10);
            ExtentReports.appendToReport(getBrowser());
            setText(getBrowser(), By.id("mailingAddress3"), formEmpresa.get("Complemento"), 10);
            setText(getBrowser(), By.id("mailingaddress4"), formEmpresa.get("Bairro"), 10);
            setText(getBrowser(), By.id("mailingPhone"), formEmpresa.get("TelefoneEmpresa"), 10);

            Select dropDownState = new Select(getBrowser().findElement(By.id("mailingState")));
            dropDownState.selectByValue(formEmpresa.get("Estado"));

            //setText(getBrowser(), By.id("mailingState"),    formEmpresa.get("Estado"), 1);
            setText(getBrowser(), By.id("mailingCity"), formEmpresa.get("Cidade"), 10);
            setText(getBrowser(), By.id("mailingZip"), formEmpresa.get("CEP"), 10);
        }



        //Checkbox - Mantenha os mesmos detalhes da cobrança
        clickOnElement(getBrowser(), By.id("billingCheck"), 10);

        //Fuso Horário
        Select dropDownFuso = new Select(getBrowser().findElement(By.id("timeZone")));
        dropDownFuso.selectByIndex(1);


        //Checkbox - Eu concordo
        clickOnElement(getBrowser(), By.id("i_agree"), 10);

        //Botão Cadastrar
        clickOnElement(getBrowser(), By.id("btn_submit"), 10);


        try{

            String mensagem = getText(getBrowser(), By.xpath("//div[@class='alignCenter RegisterSuccessText']"), 5);

            if (mensagem.equals("Obrigado por se cadastrar!")) {
                Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.xpath("//div[@class='alignCenter RegisterSuccessText']")));
                ExtentReports.appendToReport(getBrowser());
            } else {
                throw new java.lang.Error("YOU GOT AN ERROR");
            }

        }
        catch(Exception e){

            clickOnElement(getBrowser(), By.xpath("//div[@class='notify-msg']"), 11);

            String setEmailAddress = "vivoautotest"+randomNum(3)+"@mailinator.com";
            hashPessoaFisica.put("hashEmail", setEmailAddress);

            setText(getBrowser(), By.id("email"), setEmailAddress, 11);

            //Checkbox - Mantenha os mesmos detalhes da cobrança
            clickOnElement(getBrowser(), By.id("billingCheck"), 10);

            //Checkbox - Eu concordo
            clickOnElement(getBrowser(), By.id("i_agree"), 10);

            //Botão Cadastrar
            clickOnElement(getBrowser(), By.id("btn_submit"), 10);

        }
    }

    @E("^ao finalizar o cadastro com a mensagem \"([^\"]*)\" será exibida$")
    public void aoFinalizarOCadastroComAMensagemSeráExibida(String texto) {

        String mensagem = getText(getBrowser(), By.xpath("//div[@class='alignCenter RegisterSuccessText']"), 10);

        if (mensagem != texto) {
            Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.xpath("//div[@class='alignCenter RegisterSuccessText']")));
            ExtentReports.appendToReport(getBrowser());
        } else {
            throw new java.lang.Error("YOU GOT AN ERROR");
        }
    }

    @Quando("^Eu acessar o menu \"([^\"]*)\" e o filtro \"([^\"]*)\" e \"([^\"]*)\"$")
    public void euAcessarOMenuEOFiltroE(String itemMenu1, String itemFiltro1, String itemFiltro2) {

        clickOnElement(getBrowser(), By.xpath("//a[@class='main-link no-sublink'][contains(text(),'" + itemMenu1 + "')]"), 11);
        //Verifications.wait(2);

        //CHECKBOXES - TIPOS DE SERVIÇO
        if (itemFiltro1.equals("SaaS")) {
            clickOnElement(getBrowser(), By.id("ajax-facets-checkboxes-field-family-service-type-139"), 11);
            Verifications.wait(2);
        } else {
            clickOnElement(getBrowser(), By.id("ajax-facets-checkboxes-field-family-service-type-144"), 11);
        }

        //CHECKBOXES - CATEGORIA
        switch (itemFiltro2) {
            case "Produtividade":
                clickOnElement(getBrowser(), By.id("ajax-facets-checkboxes-field-family-category-250"), 11);
                break;
            case "Gerenciamento":
                clickOnElement(getBrowser(), By.id("ajax-facets-checkboxes-field-family-category-249"), 11);
                break;
            case "Compartilhamento":
                clickOnElement(getBrowser(), By.id("ajax-facets-checkboxes-field-family-category-248"), 10);
                break;
            case "Segurança":
                clickOnElement(getBrowser(), By.id("ajax-facets-checkboxes-field-family-category-251"), 10);
                break;
            case "Comunicação":
                clickOnElement(getBrowser(), By.id("ajax-facets-checkboxes-field-family-category-246"), 10);
                break;
            case "Mobilidade":
                clickOnElement(getBrowser(), By.id("ajax-facets-checkboxes-field-family-category-245"), 10);
                break;
            case "Armazenamento":
                clickOnElement(getBrowser(), By.id("ajax-facets-checkboxes-field-family-category-198"), 10);
                break;
            case "Computação":
                clickOnElement(getBrowser(), By.id("ajax-facets-checkboxes-field-family-category-200"), 10);
                break;
        }
        Verifications.wait(2);
        ExtentReports.appendToReport(getBrowser());

    }


    @E("^ao selecionar o produto \"([^\"]*)\" e depois \"([^\"]*)\"$")
    public void aoSelecionarOProdutoEDepois(String produto, String subProduto) {

        //SELECIONA PRODUTO
        clickOnElement(getBrowser(), By.xpath("//div[@class='titleContainer views-fieldset']//span[contains(text(),'" + produto + "')]"), 1);
        clickOnElement(getBrowser(), By.xpath("//div[@class='view-content row']//div[1]//div[1]//div[1]//div[1]//div[2]//div[3]//span[1]//a[1]"), 1);
        //Verifications.wait(2);

        //SELECIONA SUB-PRODUTO
        if (produto.equals("Microsoft 365")) {
            switch (subProduto) {
                case "Microsoft 365 F1":
                    Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.xpath("//h2[contains(text(),'" + subProduto + "')]")));
                    clickOnElement(getBrowser(), By.xpath("//div[contains(@class,'item active')]//div[1]//a[1]"), 11);
                    ExtentReports.appendToReport(getBrowser());
                    break;
                case "Microsoft 365 Business":
                    Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.xpath("//div[@id='mainWrapper']//div[2]//a[1]")));
                    clickOnElement(getBrowser(), By.xpath("//div[@id='mainWrapper']//div[2]//a[1]"), 11);
                    ExtentReports.appendToReport(getBrowser());
                    break;
                case "Microsoft 365 E3":
                    Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.xpath("//div[contains(@class,'group-one-sidebar group-sidebar-left clearfix')]//div[3]//a[1]")));
                    clickOnElement(getBrowser(), By.xpath("//div[contains(@class,'group-one-sidebar group-sidebar-left clearfix')]//div[3]//a[1]"), 11);
                    ExtentReports.appendToReport(getBrowser());
                    break;
                case "Microsoft 365 E5":
                    Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.xpath("//div[contains(@class,'innerPage bg-3 service_family')]//div[4]//a[1]")));
                    clickOnElement(getBrowser(), By.xpath("//div[contains(@class,'innerPage bg-3 service_family')]//div[4]//a[1]"), 11);
                    ExtentReports.appendToReport(getBrowser());
                    break;
            }
        }
    }

    @E("^seguir com os itens \"([^\"]*)\"$")
    public void seguirComOsItens(String btnFinalizar) {

        //BOTÃO: FINALIZAR COMPRA
        clickOnElement(getBrowser(), By.xpath("//span[contains(text(),'Finalizar Compra')]"), 20);
        Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.xpath("//span[contains(text(),'Finalizar Compra')]")));
        ExtentReports.appendToReport(getBrowser());
    }

    @E("^validar em \"([^\"]*)\" e \"([^\"]*)\"$")
    public void validarEmE(String btnContinueNav, String btnProsseguir) {


        if (hashUsuarios.equals("Novo Cliente")){



            try{

                for (String winHandle : getBrowser().getWindowHandles()) {
                    getBrowser().switchTo().window(winHandle);
                }
                //BOTÃO: CONTINUAR NAVEGANDO
                clickOnElement(getBrowser(), By.xpath("//div[@class='content']//div[@id='validateCreditContainer']//div[@id='continueBt']//div//button[@class='active']"), 20);

                //BOTÃO CONTINUAR - ALERTA DE DEPENDÊNCIA DE SERVIÇO
                clickOnElement(getBrowser(), By.xpath("//button[@id='actioncontinue']//span[contains(text(),'" + btnContinueNav + "')]"), 11);
            }
            catch (Exception e){

                for (String winHandle : getBrowser().getWindowHandles()) {
                    getBrowser().switchTo().window(winHandle);
                }
                //BOTÃO CONTINUAR - ALERTA DE DEPENDÊNCIA DE SERVIÇO
                clickOnElement(getBrowser(), By.xpath("//button[@id='actioncontinue']//span[contains(text(),'" + btnContinueNav + "')]"), 11);

            }

            //BOTÃO FINALIZAR COMPRA
            clickOnElement(getBrowser(), By.xpath("//span[contains(text(),'Finalizar Compra')]"), 20);

            for (String winHandle : getBrowser().getWindowHandles()) {
                getBrowser().switchTo().window(winHandle);
            }

            //BOTÃO CONCORDO COM TUDO - CONTRATO DE SERVIÇO
            clickOnElement(getBrowser(), By.xpath("//span[contains(text(),'Concordo com Tudo')]"), 21);

            //INFORMAÇÃO ADICIONAL
            setText(getBrowser(), By.id("domain"), txtMicrosoftDomain,11);

            //BOTÃO SALVAR E PRÓXIMO - INFORMAÇÃO ADICIONAL - 1ª AÇÃO
            clickOnElement(getBrowser(), By.id("btnSaveNext"), 21);

            //BOTÃO SALVAR E PRÓXIMO - INFORMAÇÃO ADICIONAL - 2ª AÇÃO
            clickOnElement(getBrowser(), By.id("btnSaveFinish"), 21);


            //SELECIONAR O CHECKBOX - BOLETO
            clickOnElement(getBrowser(), By.id("billingMapBLT"), 11);

            ExtentReports.appendToReport(getBrowser());

            //BOTÃO: PROSSEGUIR
            clickOnElement(getBrowser(), By.id("btnProceed"), 21);

        }else {

            ExtentReports.appendToReport(getBrowser());

            //BOTÃO: PROSSEGUIR
            clickOnElement(getBrowser(), By.id("btnProceed"), 21);
        }




    }


    @Então("^confirmar o pagamento pela mensagem \"([^\"]*)\"$")
    public void confirmarOPagamentoPelaMensagem(String txtCompraFinalizada) {
        Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.xpath("//h2[contains(text(),'Pedido Concluído')]")));

        String compareTxt = getText(getBrowser(), By.xpath("//div[@class='notify-msg']"), 11);

        if (compareTxt.equals(txtCompraFinalizada)) {
            Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.xpath("//div[@class='notify-msg']")));
            ExtentReports.appendToReport(getBrowser());
        } else {
            throw new java.lang.Error("YOU GOT AN ERROR");
        }
    }

    @E("^ao logar na conta de email abra o link para Alteração de Senha enviado no fim do cadastro$")
    public void aoLogarNaContaDeEmailAbraOLinkParaAlteraçãoDeSenhaEnviadoNoFimDoCadastro() {

        getBrowser().get("https://www.mailinator.com/");

        setText(getBrowser(), By.id("addOverlay"), hashPessoaFisica.get("hashEmail").toString(), 11);
        clickOnElement(getBrowser(), By.id("go-to-public"), 11);
        clickOnElement(getBrowser(), By.xpath("//td[contains(text(),'no-reply@dev.vivoplataformadigital.com.br')]"), 11);

        getBrowser().switchTo().defaultContent();
        getBrowser().switchTo().frame("msg_body");

        Actions act = new Actions(getBrowser());
        act.sendKeys(Keys.PAGE_DOWN).build().perform();
        Verifications.wait(1);
        act.sendKeys(Keys.PAGE_DOWN).build().perform();
        ExtentReports.appendToReport(getBrowser());

        Verifications.wait(1);
        clickOnElement(getBrowser(), By.xpath("//a[contains(text(),'clique aqui.')]"),11);
    }


    @Então("^ao definir a senha e os demais dados solicitados clique em \"([^\"]*)\"$")
    public void aoDefinirASenhaEOsDemaisDadosSolicitadosCliqueEm(String btnSalvar){

        for (String winHandle : getBrowser().getWindowHandles()){
            getBrowser().switchTo().window(winHandle);
        }

        setText(getBrowser(), By.id("newPassword"), "Vivoteste2020", 11); //nova senha
        setText(getBrowser(), By.id("confirmPassword"), "Vivoteste2020", 11); //confirmação de senha
        //pergunta de segurança

        Select dropDownPerguntaSeguranca = new Select(getBrowser().findElement(By.id("securityQuestion")));
        dropDownPerguntaSeguranca.selectByIndex(2);


        setText(getBrowser(), By.id("securityQueAns"), "Roxo", 11); //resposta de segurança

        //BOTÃO SALVAR E FINALIZAR
        clickOnElement(getBrowser(), By.xpath("//span[contains(text(),'"+btnSalvar+"')]"), 11);

        clickOnElement(getBrowser(), By.xpath("//div[@class='notify-msg']"), 11);
        Verifications.highlightElementfixed(getBrowser(), getBrowser().findElement(By.xpath("//div[@class='notify-msg']")));
        ExtentReports.appendToReport(getBrowser());
    }


}



