package br.com.pom.interfaces;

public interface Environment {

   String endPointDevCA  = "http://dev.cambridgeassociates.com";

   String hangfireDevCA = "http://irodrigues:Powerslave84@dev.cambridgeassociates.com/datastream/hangfireAdmin/";

   String testingPage = "http://automationpractice.com/index.php";

   String DevRslVivo = "https://rsl-dev.vivoplataformadigital.com.br/";

   String mailinatorAddress = "https://www.mailinator.com/";

}
