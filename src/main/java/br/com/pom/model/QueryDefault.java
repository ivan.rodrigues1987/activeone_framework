package br.com.pom.model;

import javax.management.Query;
import java.sql.ResultSet;

public class QueryDefault {

    public QueryDefault() {

    }

    public String shortBenchmarkSetConfig(String jobName) {
        String sqlQuery = "SELECT BenchmarkSetId, source_Id, vendor_id, isActive, FrequencyTypeId, Day, ReferenceAsOfDateTypeId, TimeOfDay, CenterCode, CountryCode FROM ExposureData.BenchmarkSetConfig WHERE benchmarkSetID = '" + jobName + "'";

        return sqlQuery;
    }

    public String shortBenchmark(String jobName){
        String sqlQuery = "SELECT TOP 30 BenchmarkId, Date, Currency, Name, MarketCap, Price, EntrySourceId FROM ExposureData.Benchmark WHERE BenchmarkId IN ("+jobName+") and EntrySourceId = 2 ORDER BY BenchmarkId, Date asc";

        return sqlQuery;
    }

    public String checkApplicationLog(String jobId, String status){
        String sqlQuery = "SELECT LogId, EntryDate, Origin, LogLevel, Message, JobId FROM CMDBDataDownload.ApplicationLog WHERE JobId = '" + jobId + "' AND Message = 'Status: " + status + "'";

        return sqlQuery;

    }

    public String rollUpConstituent (String benchmarkId, String familyId, String currency, String date){
        String sqlQuery = "SELECT bsb.BenchmarkSetId, bsb.BenchmarkId, const.FamilyId, const.Date, const.Currency, SUM(const.Weight) as RollUpTo100 FROM ExposureData.Constituent const INNER JOIN ExposureData.BenchmarkSetBenchmarks bsb ON const.BenchmarkId = bsb.BenchmarkId WHERE const.BenchmarkId IN (" + benchmarkId + ") AND const.FamilyId = '"+familyId+"' AND const.Currency = '"+currency+"' AND const.Date = '"+date+"' GROUP BY bsb.BenchmarkSetId, bsb.BenchmarkId, const.Date, const.Currency, const.FamilyId";

        return sqlQuery;
    }

    public String getJobId (String getJobId){
        String sqlQuery = "SELECT JobId FROM CMDBDataDownload.ApplicationLog WHERE JobId LIKE '%"+getJobId+".%' ORDER BY LogId desc";

        return sqlQuery;
    }
}

