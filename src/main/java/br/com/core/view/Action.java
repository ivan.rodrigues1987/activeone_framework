package br.com.core.view;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.Random;

/**
 * @author
 * @since 13/05/2018
 */
public class Action {

    /**
     * Returns an existing element from the screen
     *
     * @param by      Type of "By"
     * @param seconds Waits for the defined time set as parameter
     * @param driver  driver of the application
     * @return Returns an existing element from the screen
     * @author
     */
    public static WebElement getExistingElement(WebDriver driver, By by, int seconds) {
        return new WebDriverWait(driver, seconds).until(ExpectedConditions.presenceOfElementLocated(by));
    }


    /**
     * Returns a visible element from the screen
     *
     * @param by      Type of "By"
     * @param seconds Waits for the defined time set as parameter
     * @param driver  driver of the application
     * @return Returns an visible element from the screen
     * @author
     */
    public static WebElement getVisibleElement(WebDriver driver, By by, int seconds) {
        return new WebDriverWait(driver, seconds).until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    /**
     * Returns a clickable element from the screen
     *
     * @param by      Type of "By"
     * @param seconds Waits for the defined time set as parameter
     * @param driver  driver of the application
     * @return Returns a clickable element from the screen
     * @author
     */
    public static WebElement getClickableElement(WebDriver driver, By by, int seconds) {
        return new WebDriverWait(driver, seconds).until(ExpectedConditions.elementToBeClickable(by));
    }

    public static boolean getElementNotPresent(WebDriver driver, By by, int seconds) {
        return new WebDriverWait(driver, seconds).until(ExpectedConditions.not(ExpectedConditions.presenceOfAllElementsLocatedBy(by)));
    }


    /**
     * Clears the Text Field of a WebElement
     *
     * @param by      Type of "By"
     * @param seconds Waits for the defined time set as parameter
     * @param driver  driver of the application
     * @author
     */
    public static void clearField(WebDriver driver, By by, int seconds) {
        WebElement element = getClickableElement(driver, by, seconds);
        element.clear();
    }

    /**
     * Enters text in a WebElement Text Field
     *
     * @param driver  driver of the application
     * @param by      Type of "By"
     * @param text    set a text to web element
     * @param seconds Waits for the defined time set as parameter
     * @author
     */
    public static void setText(WebDriver driver, By by, String text, int seconds) {
        WebElement element = getClickableElement(driver, by, seconds);
        element.click();
        element.clear();
        element.sendKeys(text);
    }

    /**
     * Gets text from a WebElement Text Field
     *
     * @param driver  driver of the application
     * @param by      Type of "By"
     * @param seconds Waits for the defined time set as parameter
     * @author
     */
    public static String getText(WebDriver driver, By by, int seconds) {

        String textExtracted = null;
        WebElement element = Action.getVisibleElement(driver, by, seconds);
        if (element != null) {
            textExtracted = element.getText().trim();
        } else {
            Assert.fail("Failed to retrieve the value from the web element.");
        }
        return textExtracted;
    }


    /**
     * Gets text by attribute in a WebElement Text Field
     *
     * @param driver    driver of the application
     * @param by        Type of "By"
     * @param seconds   Waits for the defined time set as parameter
     * @param attribute get text value thought of the tag name
     * @author
     */
    public static String getTextAttribute(WebDriver driver, By by, String attribute, int seconds) {

        String textExtracted = null;
        WebElement element = Action.getVisibleElement(driver, by, seconds);
        if (element != null) {
            textExtracted = element.getAttribute(attribute).trim();
        } else {
            Assert.fail("Failed to retrieve the value from the web element.");
        }
        return textExtracted;
    }


    /**
     * Clicks at the web element
     *
     * @param driver  driver of the application
     * @param by      Type of "By"
     * @param seconds Waits for the defined time set as parameter
     * @author
     */
    public static void clickOnElement(WebDriver driver, By by, int seconds) {
        //if the object is not enabled or visible, this line finalizes the test, but if the object exists the method returns a AppWeb Element object
        WebElement element = getClickableElement(driver, by, seconds);
        //Clicks at the element requested
        element.click();
    }

    public static String gerarCPF() {

        int digito1 = 0, digito2 = 0, resto = 0;
        String nDigResult;
        String numerosContatenados;
        String numeroGerado;

        Random numeroAleatorio = new Random();


        //numeros gerados
        int n1 = numeroAleatorio.nextInt(10);
        int n2 = numeroAleatorio.nextInt(10);
        int n3 = numeroAleatorio.nextInt(10);
        int n4 = numeroAleatorio.nextInt(10);
        int n5 = numeroAleatorio.nextInt(10);
        int n6 = numeroAleatorio.nextInt(10);
        int n7 = numeroAleatorio.nextInt(10);
        int n8 = numeroAleatorio.nextInt(10);
        int n9 = numeroAleatorio.nextInt(10);


        int soma = n9 * 2 + n8 * 3 + n7 * 4 + n6 * 5 + n5 * 6 + n4 * 7 + n3 * 8 + n2 * 9 + n1 * 10;

        int valor = (soma / 11) * 11;

        digito1 = soma - valor;

        //Primeiro resto da divisão por 11.
        resto = (digito1 % 11);

        if (digito1 < 2) {
            digito1 = 0;
        } else {
            digito1 = 11 - resto;
        }

        int soma2 = digito1 * 2 + n9 * 3 + n8 * 4 + n7 * 5 + n6 * 6 + n5 * 7 + n4 * 8 + n3 * 9 + n2 * 10 + n1 * 11;

        int valor2 = (soma2 / 11) * 11;

        digito2 = soma2 - valor2;

        //Primeiro resto da divisão por 11.
        resto = (digito2 % 11);

        if (digito2 < 2) {
            digito2 = 0;
        } else {
            digito2 = 11 - resto;
        }

        //Conctenando os numeros
        numerosContatenados = String.valueOf(n1) + String.valueOf(n2) + String.valueOf(n3) + "." + String.valueOf(n4) +
                String.valueOf(n5) + String.valueOf(n6) + "." + String.valueOf(n7) + String.valueOf(n8) +
                String.valueOf(n9) + "-";

        //Concatenando o primeiro resto com o segundo.
        nDigResult = String.valueOf(digito1) + String.valueOf(digito2);

        numeroGerado = numerosContatenados + nDigResult;

        System.out.println("CPF Gerado " + numeroGerado);

        return numeroGerado;
    }//fim do metodo geraCPF


    public static String gerarCNPJ() {

        int digito1 = 0, digito2 = 0, resto = 0;
        String nDigResult;
        String numerosContatenados;
        String numeroGerado;

        Random numeroAleatorio = new Random();


        //numeros gerados
        int n1 = numeroAleatorio.nextInt(10);
        int n2 = numeroAleatorio.nextInt(10);
        int n3 = numeroAleatorio.nextInt(10);
        int n4 = numeroAleatorio.nextInt(10);
        int n5 = numeroAleatorio.nextInt(10);
        int n6 = numeroAleatorio.nextInt(10);
        int n7 = numeroAleatorio.nextInt(10);
        int n8 = numeroAleatorio.nextInt(10);
        int n9 = numeroAleatorio.nextInt(10);
        int n10 = numeroAleatorio.nextInt(10);
        int n11 = numeroAleatorio.nextInt(10);
        int n12 = numeroAleatorio.nextInt(10);


        int soma = n12 * 2 + n11 * 3 + n10 * 4 + n9 * 5 + n8 * 6 + n7 * 7 + n6 * 8 + n5 * 9 + n4 * 2 + n3 * 3 + n2 * 4 + n1 * 5;

        int valor = (soma / 11) * 11;

        digito1 = soma - valor;

        //Primeiro resto da divisão por 11.
        resto = (digito1 % 11);

        if (digito1 < 2) {
            digito1 = 0;
        } else {
            digito1 = 11 - resto;
        }

        int soma2 = digito1 * 2 + n12 * 3 + n11 * 4 + n10 * 5 + n9 * 6 + n8 * 7 + n7 * 8 + n6 * 9 + n5 * 2 + n4 * 3 + n3 * 4 + n2 * 5 + n1 * 6;

        int valor2 = (soma2 / 11) * 11;

        digito2 = soma2 - valor2;

        //Primeiro resto da divisão por 11.
        resto = (digito2 % 11);

        if (digito2 < 2) {
            digito2 = 0;
        } else {
            digito2 = 11 - resto;
        }

        //Conctenando os numeros
        numerosContatenados = String.valueOf(n1) + String.valueOf(n2) + "." + String.valueOf(n3) + String.valueOf(n4) +
                String.valueOf(n5) + "." + String.valueOf(n6) + String.valueOf(n7) + String.valueOf(n8) + "/" +
                String.valueOf(n9) + String.valueOf(n10) + String.valueOf(n11) +
                String.valueOf(n12) + "-";

        //Concatenando o primeiro resto com o segundo.
        nDigResult = String.valueOf(digito1) + String.valueOf(digito2);

        numeroGerado = numerosContatenados + nDigResult;

        System.out.println("Digito 2 ->" + digito2);

        System.out.println("CNPJ Gerado " + numeroGerado);

        return numeroGerado;
    }


    public static String gerarRg() {

        String nDigResult;
        String numerosContatenados;
        String numeroGerado;

        Random numeroAleatorio = new Random();


        //numeros gerados
        int n1 = numeroAleatorio.nextInt(10);
        int n2 = numeroAleatorio.nextInt(10);
        int n3 = numeroAleatorio.nextInt(10);
        int n4 = numeroAleatorio.nextInt(10);
        int n5 = numeroAleatorio.nextInt(10);
        int n6 = numeroAleatorio.nextInt(10);
        int n7 = numeroAleatorio.nextInt(10);
        int n8 = numeroAleatorio.nextInt(10);
        int n9 = numeroAleatorio.nextInt(10);


        //Conctenando os numeros
        numerosContatenados = String.valueOf(n1) + String.valueOf(n2) + String.valueOf(n3) + String.valueOf(n4) +
                String.valueOf(n5) + String.valueOf(n6) + String.valueOf(n7) + String.valueOf(n8) +
                String.valueOf(n9);

        numeroGerado = numerosContatenados;

        return numeroGerado;
    }


    public static String gerarSenha() {
        String letras = "ABCDEFGHIJKLMNOPQRSTUVYWXZabcdefghijklmnopqrstuvywxz0123456789";

        Random random = new Random();

        String armazenaSenha = "";
        int index = -1;
        for (int i = 0; i < 9; i++) {
            index = random.nextInt(letras.length());
            armazenaSenha += letras.substring(index, index + 1);

        }
        return armazenaSenha;
    }

    public static String randomNum(int nChar){

        Random random = new Random();
        char[] numero = "0123456789".toCharArray();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < nChar; i++){
            int num = random.nextInt(numero.length);
            sb.append(numero[num]);
        }

       return sb.toString();

    }
}

