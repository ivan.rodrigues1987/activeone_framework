package br.com.core.asserts;


import br.com.core.database.DataBases;
import br.com.core.enums.DbPlatform;
import br.com.core.ftp.SftpManager;
import br.com.core.report.ExtentReports;
import br.com.pom.model.QueryDefault;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.openqa.selenium.WebDriver;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


public class CAValidations {

    public static WebDriver driver;
    public static Response responseIndexRate;
    public static RestAssured restAssuredIndexRate;
    public static Response responseExposureData;
    public static RestAssured restAssuredExposureData;

    public static Map<String, Object> hashIndexRate = new HashMap<String, Object>();
    public static Map<String, Object> hashExposureData = new HashMap<String, Object>();
    //private static Map<String, Object> hashURI = new HashMap<String, Object>();
    public static Map<String, Object> hashExposureDataFields = new HashMap<>();
    public static Map<String, Object> hashJobType = new HashMap<>();
    public static Map<String, Object> hashResultSet = new HashMap<>();
    public static Map<String, Object> hashTableName = new HashMap<>();


    public static void getJobId()throws SQLException {

        //OPENS DATABASE CONNECTION
        DataBases db = new DataBases();
        db.openConnection(DbPlatform.SQLSERVER);

        //OBJECT TO RETRIEVE THE QUERIES FROM THE QUERY PageObject CLASS.
        QueryDefault call = new QueryDefault();

        if (hashIndexRate.get("IndexRate").equals("Index Rate")){
            call.getJobId(hashIndexRate.get("jobType").toString());
            ResultSet rs = db.select(call.getJobId(hashIndexRate.get("jobType").toString()));

            while (rs.next()) {
                hashResultSet.put("getJobId", rs.getString(1));
                ExtentReports.appendToReport("JobId: " + hashResultSet.get("getJobId").toString());
                break;
            }
        }else
        {
            call.getJobId(hashExposureData.get("benchmarkSet").toString());
            ResultSet rs = db.select(call.getJobId(hashExposureData.get("benchmarkSet").toString()));

            while (rs.next()) {
                hashResultSet.put("getJobId", rs.getString(1));
                ExtentReports.appendToReport("JobId: " + hashResultSet.get("getJobId").toString());
                break;
            }
        }
    }

    public void sftpConnection() {
        SftpManager sftpManager = new SftpManager("upload.cambridgeassociates.com", 22, "svc-p-factset", "w%6^9%05(4c-39R)2=2Q0s1(N78)-waU2$88g*oee09D");
        sftpManager.openConnection();
        sftpManager.downloadFile("/datafeeds/benchmarks/bdf_aasx_20191007.zip", "target\\bdf_aasx_20191007.zip");
    }

}
